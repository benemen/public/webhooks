# Webhooks

This document describes Benemen webhook implementation, providing integrators push notifications about events in Benemen core systems. This functionality supplements BeneAPI, which is a RESTful API relying on request-response pattern unsuitable for notifications.

Simply put, webhooks send a HTTPS POST request to predefined URL whenever defined events occur in the system. The request contains JSON data providing information about the event instance.

For example, if the notification is about an incoming service pool call, the data might include arrival timestamp, caller number and target service pool. This information can then be used in the receiving end to perform any additional logic relating to the notification.

Some possible use scenarios include:

- Receiving notifications about completed service pool calls to send SMS feedback questionnaires
- Receiving notifications about service pool allocations to agents to display custom information from an internal system on their workstations
- Generate contact events to a CRM system about completed calls

Full documentation can be found at https://doc.beneservices.com/webhooks/.
